import pandas as pd
import sqlalchemy
import psycopg2
import os, io
from time import perf_counter
from config import *

PREP_TABLE = (
                """
                DROP TABLE IF EXISTS tradingops.marketdata;
                """,
                """
                CREATE TABLE tradingops.marketdata
                (
                    id BIGSERIAL,
                    "timestamp" TIMESTAMP WITHOUT TIME ZONE NOT NULL,                    
                    "open" REAL NOT NULL,
                    "high" REAL NOT NULL,
                    "low" REAL NOT NULL,
                    "close" REAL NOT NULL,
                    "volume" BIGINT NOT NULL,
                    "numtrades" BIGINT NOT NULL,
                    "bid_volume" BIGINT NOT NULL,
                    "ask_volume" BIGINT NOT NULL,
                    "datafreq" SMALLINT NOT NULL, --in seconds
                    "symbol" TEXT NOT NULL,
                    PRIMARY KEY ("symbol", "datafreq", "timestamp")
                );
                """
            )

def prepare_table():
    try:
        conn_create = psycopg2.connect(database=DB_NAME, user=DB_USERNAME, password=DB_PASSWORD, host=DB_HOST)
        for stmnt in PREP_TABLE:
            cur = conn_create.cursor()
            cur.execute(stmnt)
            conn_create.commit()
            cur.close()

    except Exception as e:
        print(e)

    finally:
        if conn_create:
            conn_create.close()

def upload_to_tradingops(): 
    conn_copy= psycopg2.connect(database=DB_NAME, user=DB_USERNAME, password=DB_PASSWORD, host=DB_HOST)
    cur = conn_copy.cursor()
    cur.execute('truncate table tradingops.marketdata')
    conn_copy.commit()
    cur.close()
    
    for file in os.listdir(MARKETDATA_FOLDER):
        symbol = file.split('.')[0]
        print(f"Inserting {symbol}")
        rawdata = pd.read_csv(MARKETDATA_FOLDER+file, header=0, skipinitialspace = True, parse_dates=[['Date','Time']])
        rawdata['datafreq'] = 5
        rawdata['symbol'] = symbol
        rawdata.columns = ['timestamp','open','high','low','close','volume','numtrades','bid_volume','ask_volume','datafreq','symbol']
        rawdata['timestamp'] = rawdata['timestamp'].dt.tz_localize('US/Eastern').dt.tz_convert('UTC')
        rawdata.drop_duplicates(subset = 'timestamp', keep='last',inplace= True)
        b = io.StringIO()
        rawdata.to_csv(b, sep= ',', header=False, index=True)
        b.seek(0)
        cur = conn_copy.cursor()
        sqlstate = "COPY tradingops.marketdata FROM STDIN WITH CSV HEADER DELIMITER AS ','"        
        cur.copy_expert(sql = sqlstate , file = b)
        conn_copy.commit()
        cur.close()

if __name__ == "__main__":
    start = perf_counter()
    prepare_table()
    upload_to_tradingops()
    print(f"{perf_counter() - start} seconds taken")